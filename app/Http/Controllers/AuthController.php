<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function bio(){
        return view('halaman.form');
    }
    
    public function welcome(Request $request){
        $fname = $request['FName'];
        $lname = $request['LName'];       
        return view('halaman.welcome', compact('fname', 'lname'));
    }
    
    //
}
