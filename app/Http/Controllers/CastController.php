<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\cast;

class CastController extends Controller
{
    public function create()
    {
        return view ('cast.create');
    }

    public function store(Request $request)
    {
        //dd($request->all());
        $request->validate([
            'nama' => 'required',
            'bio' => 'required',
            'umur' => 'required',
            ],
            [
            'nama.required' => 'Nama tidak boleh kosong',
            'bio.required'  => 'Biografi tidak boleh kosong',
            'umur.required' => 'Umur tidak boleh kosong',
            ]
            );

            $cast = new cast;

            $cast->nama =$request->nama;
            $cast->bio =$request->bio;
            $cast->umur =$request->umur;

            $cast->save();

            return redirect('/cast');

        //return view ('cast.store');
    }
    
    public function index()
    {
        
        $cast = Cast::all();
        //dd($cast);
        return view ('cast.index', compact('cast'));
    }
    
    public function show($cast_id)
    {
        $cast = Cast::where('id', $cast_id)->first();
        return view ('cast.show', compact('cast'));
    }
    
    public function edit($cast_id)
    {
        $cast = Cast::where('id', $cast_id)->first();
        return view ('cast.edit', compact('cast'));
    }

    public function update(Request $request, $cast_id)
    {
        $request->validate([
            'nama' => 'required',
            'bio' => 'required',
            'umur' => 'required',
            ],
            );
        
            $cast = Cast::find($cast_id);
            
            $cast->nama =$request['nama'];
            $cast->bio =$request['bio'];
            $cast->umur =$request['umur'];

            $cast->save();

        
        return redirect ('/cast');
    }

    public function destroy($cast_id)
    {
        $cast = Cast::find($cast_id);
 
        $cast->delete();

        return redirect ('/cast');
    }
    //
}




