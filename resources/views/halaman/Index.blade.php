@extends('layout.master')

@section('judul')
Halaman Home
@endsection

@section('content')

<h1>MEDIA ONLINE</h1>
<h2>Sosial media developer</h2>

<p>belajar dan berbagi agar hidup menjadi lebih baik</p>

<h3> Benefit Join di Media Online </h3>
<ul>
  <li>Mendapatkan motivasi dari sesama para Developer</li>
  <li>Sharing knowledge</li>
  <li>Dibuat oleh calon developer terbaik</li>
</ul>

<h3>Cara bergabung ke Media Online</h3>
<ol>
  <li>Mengunjungi website ini</li>
  <li>Mendaftarkan di <a href="/register">Form Sign Up</a></li>
  <li>Selesai</li>
</ol>

@endsection